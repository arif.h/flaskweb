# flask_web/app.py

from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    return '<h1> Hello World v2 <h1>'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
